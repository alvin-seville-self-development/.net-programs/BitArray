using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using NETMouse;

namespace BitArray
{
    /// <summary>
    /// Represents a bit array.
    /// </summary>
    [Serializable]
    [DebuggerDisplay("Count = {Count}")]
    [DebuggerTypeProxy(typeof(NETMouse.Debug.CollectionDebugView))]
    public class BitArray : ICollection<bool>,
        IReadOnlyCollection<bool>,
        ICollection
    {
        /// <summary>
        /// Retrieves bit with the specified index or mutates it.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <exception cref="IndexOutOfRangeException">This exception is thrown when index is out of the current collection bounds.</exception>
        public bool this[int index]
        {
            get
            {
                if (!index.IsBetween(0, Count - 1))
                    throw new IndexOutOfRangeException(nameof(index));
                return (array[index / BitsPerInt] & (1 << (index % BitsPerInt))) != 0;
            }
            set
            {
                if (!index.IsBetween(0, Count - 1))
                    throw new IndexOutOfRangeException(nameof(index));
                if (value)
                    array[index / BitsPerInt] |= 1 << (index % BitsPerInt);
                else
                    array[index / BitsPerInt] &= ~(1 << (index % BitsPerInt));
                version++;
            }
        }

        /// <summary>
        /// The elements count.
        /// </summary>
        public int Count { get; }

        /// <summary>
        /// Determines whether the current collection is a synchronized one.
        /// </summary>
        public bool IsSynchronized => false;
        
        /// <summary>
        /// The synchronization object.
        /// </summary>
        [field: NonSerialized]
        public object SyncRoot { get; } = new object();

        /// <summary>
        /// Determines whether the current collection is a readonly one.
        /// </summary>
        public bool IsReadOnly => false;

        /// <summary>
        /// Constructs the new instance of BitArray with the specified length.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <exception cref="ArgumentOutOfRangeException">This exception is thrown when length parameter is less than 0.</exception>
        public BitArray(int length)
        {
            if (length < 0)
                throw new ArgumentOutOfRangeException(nameof(length));
            
            Count = length;
            array = new int[GetArrayLength(Count, BitsPerInt)];
        }

        /// <summary>
        /// Constructs the new instance of BitArray from the specified array which consists of ints.
        /// </summary>
        /// <param name="ints">The array which consists of ints.</param>
        /// <exception cref="ArgumentNullException">This exception is thrown when ints parameter is null.</exception>
        public BitArray(int[] ints)
        {
            if (ints is null)
                throw new ArgumentNullException(nameof(ints));
            
            Count = ints.Length * BitsPerInt;
            array = new int[ints.Length];
            ints.CopyTo(array, 0);
        }
        
        /// <summary>
        /// Constructs the new instance of BitArray from the specified array which consists of bytes.
        /// </summary>
        /// <param name="bytes">The array which consists of bytes.</param>
        /// <exception cref="ArgumentNullException">This exception is thrown when bytes parameter is null.</exception>
        public BitArray(byte[] bytes)
        {
            if (bytes is null)
                throw new ArgumentNullException(nameof(bytes));
            
            Count = bytes.Length * BytesPerInt;
            array = new int[GetArrayLength(bytes.Length, BytesPerInt)];
            int i = 0;
            int j = 0;
            while (j + 4 <= bytes.Length)
            {
                array[i++] = bytes[j] & 0xff |
                             (bytes[j + 1] & 0xff) << 8 |
                             (bytes[j + 2] & 0xff) << 16 |
                             (bytes[j + 3] & 0xff) << 24;
                j += 4;
            }

            switch (bytes.Length - j)
            {
                case 3:
                    array[i] = bytes[j] & 0xff;
                    goto case 2;
                case 2:
                    array[i] |= (bytes[j + 1] & 0xff) << 8;
                    goto case 1;
                case 1:
                    array[i] |= (bytes[j + 2] & 0xff) << 16;
                    break;
            }
        }

        /// <summary>
        /// Constructs the new instance of BitArray from the specified array which consists of bools.
        /// </summary>
        /// <param name="bools">The array which consists of bools.</param>
        /// <exception cref="ArgumentNullException">This exception is thrown when bools parameter is null.</exception>
        public BitArray(bool[] bools)
        {
            if (bools is null)
                throw new ArgumentNullException(nameof(bools));
            
            Count = bools.Length;
            array = new int[GetArrayLength(Count, BitsPerInt)];
            for (int i = 0; i < bools.Length; i++)
                if (bools[i])
                    array[i / BitsPerInt] |= 1 << (i % BitsPerInt);
        }

        /// <summary>
        /// Constructs the new instance of BitArray from the specified another instance of this class.
        /// </summary>
        /// <param name="bitArray">The another instance.</param>
        /// <exception cref="ArgumentNullException">This exception is thrown when bitArray parameter is null.</exception>
        public BitArray(BitArray bitArray)
        {
            if (bitArray is null)
                throw new ArgumentNullException(nameof(bitArray));

            Count = bitArray.Count;
            array = new int[GetArrayLength(Count, BitsPerInt)];
            for (int i = 0; i < Count; i++)
                this[i] = bitArray[i];
        }

        /// <summary>
        /// Applies and operation to the current bit array and another one.
        /// </summary>
        /// <param name="bitArray">The another instance.</param>
        /// <returns>The and operation result.</returns>
        /// <exception cref="ArgumentNullException">This exception is thrown when bitArray parameter is null.</exception>
        /// <exception cref="ArgumentException">This exception is thrown when lengths of two bit arrays are unequal.</exception>
        public BitArray And(BitArray bitArray)
        {
            if (bitArray is null)
                throw new ArgumentNullException(nameof(bitArray));
            if (Count != bitArray.Count)
                throw new ArgumentException(nameof(bitArray));

            BitArray newBitArray = new BitArray(this);
            for (int i = 0; i < Count; i++)
                newBitArray[i] &= bitArray[i];

            return newBitArray;
        }
        
        /// <summary>
        /// Applies or operation to the current bit array and another one.
        /// </summary>
        /// <param name="bitArray">The another instance.</param>
        /// <returns>The or operation result.</returns>
        /// <exception cref="ArgumentNullException">This exception is thrown when bitArray parameter is null.</exception>
        /// <exception cref="ArgumentException">This exception is thrown when lengths of two bit arrays are unequal.</exception>
        public BitArray Or(BitArray bitArray)
        {
            if (bitArray is null)
                throw new ArgumentNullException(nameof(bitArray));
            if (Count != bitArray.Count)
                throw new ArgumentException(nameof(bitArray));

            BitArray newBitArray = new BitArray(this);
            for (int i = 0; i < Count; i++)
                newBitArray[i] |= bitArray[i];

            return newBitArray;
        }

        /// <summary>
        /// Applies xor operation to the current bit array and another one.
        /// </summary>
        /// <param name="bitArray">The another instance.</param>
        /// <returns>The xor operation result.</returns>
        /// <exception cref="ArgumentNullException">This exception is thrown when bitArray parameter is null.</exception>
        /// <exception cref="ArgumentException">This exception is thrown when lengths of two bit arrays are unequal.</exception>
        public BitArray Xor(BitArray bitArray)
        {
            if (bitArray is null)
                throw new ArgumentNullException(nameof(bitArray));
            if (Count != bitArray.Count)
                throw new ArgumentException(nameof(bitArray));

            BitArray newBitArray = new BitArray(this);
            for (int i = 0; i < Count; i++)
                newBitArray[i] ^= bitArray[i];

            return newBitArray;
        }
        
        /// <summary>
        /// Applies not operation to the current bit array.
        /// </summary>
        /// <returns>The not operation result.</returns>
        public BitArray Not()
        {
            BitArray newBitArray = new BitArray(Count);
            for (int i = 0; i < Count; i++)
                newBitArray[i] = !this[i];

            return newBitArray;
        }
        
        void ICollection<bool>.Add(bool item)
        {
            throw new NotSupportedException();
        }

        void ICollection<bool>.Clear()
        {
            throw new NotSupportedException();
        }
        
        bool ICollection<bool>.Remove(bool item)
        {
            throw new NotSupportedException();
        }

        public bool Contains(bool item)
        {
            throw new NotSupportedException();
        }
        
        /// <summary>
        /// Copies the current collection into the specified array from the particular index.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">The index.</param>
        public void CopyTo(bool[] array, int arrayIndex)
        {
            CheckArrayCorrectness(array, arrayIndex);
            for (int i = 0; i < Count; i++)
                array[i + arrayIndex] = this[i];
        }

        /// <summary>
        /// Copies the current collection into the specified array from the particular index.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="index">The index.</param>
        void ICollection.CopyTo(Array array, int index)
        {
            CheckArrayCorrectness(array, index);
            if (array.Rank != 1)
                throw new RankException(nameof(array));
            if (array.GetLowerBound(0) != 0)
                throw new ArgumentException(nameof(array));
            
            for (int i = 0; i < Count; i++)
                array.SetValue(this[i], i + index);
        }

        /// <summary>
        /// Returns the enumerator of the current collection.
        /// </summary>
        /// <returns>The enumerator.</returns>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }
        
        /// <summary>
        /// Returns the enumerator of the current collection.
        /// </summary>
        /// <returns>The enumerator.</returns>
        IEnumerator<bool> IEnumerable<bool>.GetEnumerator()
        {
            return GetEnumerator();
        }
        
        /// <summary>
        /// Returns the enumerator of the current collection.
        /// </summary>
        /// <returns>The enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        
        public static BitArray operator&(BitArray left, BitArray right)
        {
            return left.And(right);
        }
        
        public static BitArray operator|(BitArray left, BitArray right)
        {
            return left.Or(right);
        }
        
        public static BitArray operator^(BitArray left, BitArray right)
        {
            return left.Xor(right);
        }
        
        public static BitArray operator~(BitArray single)
        {
            return single.Not();
        }
        
        private void CheckArrayCorrectness(Array array, int index)
        {
            if (array is null)
                throw new ArgumentNullException();
            if (!index.IsBetween(0, array.Length - 1))
                throw new ArgumentOutOfRangeException(nameof(index));
            if (array.Length - index < Count)
                throw new ArgumentException(nameof(array));
        }
        
        private int GetArrayLength(int amount, int divisor)
        {
            return amount != 0 ? (amount - 1) / divisor + 1 : 0;
        }
        
        private const int BitsPerInt = 32;
        private const int BytesPerInt = 4;
        private readonly int[] array;
        private int version;

        /// <summary>
        /// The enumerator.
        /// </summary>
        public struct Enumerator : IEnumerator<bool>
        {
            /// <summary>
            /// The current item.
            /// </summary>
            /// <exception cref="InvalidOperationException">This exception is when thrown user attempts to get item before starting enumeration or when collection changes during enumeration.</exception>
            public bool Current
            {
                get
                {
                    if (index < 0)
                        throw new InvalidOperationException(EnumerationNotStarted);
                    if (version != source.version)
                        throw new InvalidOperationException(CollectionWasChanged);
                    
                    return source[index];
                }
            }
            
            /// <summary>
            /// The current item.
            /// </summary>
            object IEnumerator.Current => Current;
            
            /// <summary>
            /// Constructs the new instance of Enumerator from the specified bit array.
            /// </summary>
            /// <param name="source">The bit array.</param>
            public Enumerator(BitArray source)
            {
                this.source = source;
                version = source.version;
                index = -1;
            }
            
            /// <summary>
            /// Moves the current enumerator on one unit and returns the value that indicates whether the futher traversing is possible.
            /// </summary>
            /// <returns>The value that indicates whether the futher traversing is possible.</returns>
            /// <exception cref="InvalidOperationException">This exception is thrown when when collection changes during enumeration.</exception>
            public bool MoveNext()
            {
                if (version != source.version)
                    throw new InvalidOperationException(CollectionWasChanged);
                
                return ++index < source.Count;
            }

            /// <summary>
            /// Resets the current enumerator.
            /// </summary>
            /// <exception cref="InvalidOperationException">This exception is thrown when when collection changes during enumeration.</exception>
            public void Reset()
            {
                if (version != source.version)
                    throw new InvalidOperationException(CollectionWasChanged);
                
                index = -1;
            }
            
            /// <summary>
            /// Releases all unmanaged resources those were used in the current enumerator.
            /// </summary>
            public void Dispose()
            {
            }
            
            private const string EnumerationNotStarted = "Enumeration hasn't started";
            private const string CollectionWasChanged = "Collection has been changed during enumeration";
            private readonly BitArray source;
            private readonly int version;
            private int index;
        }
    }
}